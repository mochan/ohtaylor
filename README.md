
## OhTaylor - Multi-Variate Taylor Series Approximation/Calculation library for Python

OhTaylor is a single function python library with just the function `taylor`. 

It can do multi-variate Taylor series approximation (R^n -> R^m) both symbolically and numerically.

## Usage
To get the Taylor series symbolically, you can pass a sympy function.

```
>>> import ohtaylor
>>> import sympy
>>> from sympy.abc import x,y,z
>>> f = sympy.exp(x**2+y)

>>> g = ohtaylor.taylor(f)

>>> sympy.pprint(next(g))
   2
 x₀  + y₀
ℯ

>>> sympy.pprint(next(g))
                 2                    2
               x₀  + y₀             x₀  + y₀
2⋅x₀⋅(x - x₀)⋅ℯ         + (y - y₀)⋅ℯ

>>> sympy.pprint(next(g))
                                             ⎛         2             2     ⎞
                          2                2 ⎜    2  x₀  + y₀      x₀  + y₀⎟
                        x₀  + y₀   (x - x₀) ⋅⎝4⋅x₀ ⋅ℯ         + 2⋅ℯ        ⎠
2⋅x₀⋅(x - x₀)⋅(y - y₀)⋅ℯ         + ───────────────────────────────────────── +
                                                       2

              2
         2  x₀  + y₀
 (y - y₀) ⋅ℯ
 ───────────────────
          2
```

Here the function is automatically creating the `x0` and `y0` as the point where the Taylor series is being evaluated.

Additionally by not giving the number of derivatives we want to calculate, it returns a generator that returns all 
terms where the total number of partial derivatives are the same.

```
>>> sympy.pprint(ohtaylor.taylor(f, x0=(0,0), n=3))
             3    2
 2      2   y    y
x ⋅y + x  + ── + ── + y + 1
            6    2
```

Here our initial point is `(0,0)` and we want up to the third derivative.

To use the numerical version, for `R->R` we just give a list of derivatives. For multi-variates `R^m->R`, we have to 
pass a dictionary that maps a tuple to a value. The tuple represents the number of partial derivative of each variable.

```python
>>> fn = {(0, 0): 1, (1, 0): 1, (0, 1): 1, (2, 0): 1, (1, 1): 1, (0, 2): 1}
>>> p = ohtaylor.taylor(fn)
>>> p((0.05, 0.05))
1.10500000000000
```
Here we give the derivatives as a tuple and value dictionary. `(1,1)` means `df2/dxdy`. Function `taylor` in 
`ohtaylor` returns a lambda that is the Taylor polynomial for the given derivatives.

For `R^n->R^m`, the functions have to given as a list and the Taylor polynomial are returned as a list. 

```
>>> f = [sympy.exp(x), sympy.sin(x)]
>>> p = ohtaylor.taylor(f, n=3)
>>> sympy.pprint(p)
⎡        3  x₀           2  x₀                                 3
⎢(x - x₀) ⋅ℯ     (x - x₀) ⋅ℯ               x₀    x₀    (x - x₀) ⋅cos(x₀)   (x
⎢───────────── + ───────────── + (x - x₀)⋅ℯ   + ℯ  , - ───────────────── - ───
⎣      6               2                                       6

     2                                     ⎤
- x₀) ⋅sin(x₀)                             ⎥
────────────── + (x - x₀)⋅cos(x₀) + sin(x₀)⎥
     2                                     ⎦
```